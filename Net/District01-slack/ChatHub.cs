﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using District01_slack.Models;

namespace District01_slack
{
    public class ChatHub : Hub
    {
        private SlackContext db = new SlackContext();

        public void Send(string name, string content, string room, string type, string time)
        {
            Message message = new Message
            {
                Name = name,
                Content = content,
                Room = room,
                Type = type,
                Timestamp = DateTime.Now
            };
            db.Messages.Add(message);
            db.SaveChanges();

            Clients.Group(room).broadcastMessage(name, content, room, type, message.Timestamp.ToString());
        }

        public void JoinRoom(string roomName)
        {
            Groups.Add(Context.ConnectionId, roomName);
        }
    }
}