﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace District01_slack.Models
{
    public class Message
    {
        public int MessageId { get; set; }
        public string Room { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string Type { get; set; }
        public DateTime Timestamp { get; set; }
    }
}