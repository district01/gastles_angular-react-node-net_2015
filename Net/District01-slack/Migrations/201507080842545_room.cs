namespace District01_slack.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class room : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "Room", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "Room");
        }
    }
}
