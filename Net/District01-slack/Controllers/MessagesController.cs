﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using District01_slack.Models;
using AttributeRouting.Web.Mvc;

namespace District01_slack.Controllers
{
    public class MessagesController : ApiController
    {
        private SlackContext db = new SlackContext();

        // GET api/Messages
        public IEnumerable<Message> GetMessages()
        {
            return db.Messages.AsEnumerable();
        }

        // GET api/Messages/room      
        [Route("api/messages/room/{room}")]
        [HttpGet]
        public IEnumerable<Message> GetMessagesForRoom(string room)
        {
            return db.Messages.Where(o => o.Room == room).AsEnumerable();
        }

        // GET api/Messages/5
        public Message GetMessage(int id)
        {
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return message;
        }

        // PUT api/Messages/5
        public HttpResponseMessage PutMessage(int id, Message message)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != message.MessageId)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(message).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Messages
        public HttpResponseMessage PostMessage(Message message)
        {
            if (ModelState.IsValid)
            {
                db.Messages.Add(message);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, message);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = message.MessageId }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Messages/5
        public HttpResponseMessage DeleteMessage(int id)
        {
            Message message = db.Messages.Find(id);
            if (message == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Messages.Remove(message);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, message);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}