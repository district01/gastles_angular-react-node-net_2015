import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import ChatConstants from '../constants/ChatConstants';
import ChatWebAPIUtils from './../utils/ChatWebAPIUtils.js';

let ActionTypes = ChatConstants.ActionTypes;

module.exports = {

    receiveAll: function(rawMessages) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.RECEIVE_RAW_MESSAGES,
            rawMessages: rawMessages
        });
    },

    receiveMessage: function(rawMessage) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.RECEIVE_RAW_MESSAGE,
            rawMessage: rawMessage
        });
    },

    receiveCreatedMessage: function(createdMessage) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.RECEIVE_RAW_CREATED_MESSAGE,
            rawMessage: createdMessage
        });
    }
};
