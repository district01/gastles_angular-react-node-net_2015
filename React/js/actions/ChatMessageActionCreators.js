import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import ChatConstants from '../constants/ChatConstants';
import ChatWebAPIUtils from './../utils/ChatWebAPIUtils.js';

let ActionTypes = ChatConstants.ActionTypes;

module.exports = {

    // Do call to api
    receiveAll: function(roomId) {
        ChatAppDispatcher.dispatch({
            type: ActionTypes.ASKED_MESSAGES,
            text: 'testj',
            currentThreadID: roomId
        });
        ChatWebAPIUtils.getAllMessagesByRoom(roomId);
    },

    joinServer: function(roomId) {
        ChatWebAPIUtils.joinServer(roomId);
    },

    socketCreateMessage: function(user, message, room) {
        ChatWebAPIUtils.socketCreateMessage(user, message, room);
    }

};
