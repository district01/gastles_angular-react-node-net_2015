// vendor
import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin'; injectTapEventPlugin();
import $ from 'jquery';

// Routing
import Router from 'react-router';
import { DefaultRoute, Link, Route, RouteHandler } from 'react-router';

// import routes
import ChatRoomApp from './components/ChatRoomApp.js';
import Home from './components/Home.js';

// Material-ui
import mui from 'material-ui';
let ThemeManager = new mui.Styles.ThemeManager();
let Colors = mui.Styles.Colors;

// Components
import Header from './components/Header.js';
import Login from './components/Login.js';

// Stores
import UserStore from './stores/UserStore.js';

// Actions
import UserActionCreators from './actions/UserActionCreators.js';

// Sockets
$.connection.hub.url = "http://slackie.azurewebsites.net/signalr";

// Set initial userStore
UserActionCreators.receiveLoggedInUser();

// get State
function getStateFromStores() {
    return {
        loggedInUser: UserStore.getLoggedInUser()
    };
}

// Base component
let App = React.createClass({

    // Setup Material-ui
    childContextTypes: {
        muiTheme: React.PropTypes.object,
    },

    // Setup Material-ui
    getChildContext() {
        return {
            muiTheme: ThemeManager.getCurrentTheme(),
        };
    },

    // Setup Material-ui
    componentWillMount() {
        ThemeManager.setPalette({
            accent1Color: Colors.deepOrange500,
        });
    },

    getInitialState: function() {
        return getStateFromStores();
    },

    // If you need to interact with the browser, perform your work in componentDidMount()
    componentDidMount: function() {
        UserStore.addChangeListener(this._onUserStoreChange);
    },

    componentWillUnmount: function() {
        UserStore.removeChangeListener(this._onUserStoreChange);
    },

    // Render Base-view
    render() {

        // If user is loggedin --> start routing else --> show login
        let content = <Login />;
        if(this.state.loggedInUser) {
            content = <RouteHandler />;
        }

        return (
            <div>
                {/* Always show header and nav */}
                <Header />

                {/* this is the important part */}
                <div className="content">
                    {content}
                </div>
            </div>
        );
    },

    /**
     * Event handler for 'change' events coming from the MessageStore
     */
    _onUserStoreChange: function() {
        this.setState(getStateFromStores());
    }
});

// Routing
let routes = (
    <Route name="app" path="/" handler={App}>
        <Route name="home" path="/" handler={Home}/>
        <Route name="room" path="/room/:id/:name" handler={ChatRoomApp}/>
    </Route>
);

Router.run(routes, function (Handler) {
        React.render(<Handler/>, document.getElementById('react'));
});