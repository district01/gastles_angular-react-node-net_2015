import ChatServerActionCreators from '../actions/ChatServerActionCreators';

/**
 * Handles communication between client and server using XMLHttpRequests and sockets
 */
module.exports = {

    // GET XMLHttpRequest, excecutes actionCreator
    getAllMessagesByRoom: function(roomId) {
        // retrieve data from a database
        $.get( "http://slackie.azurewebsites.net//api/messages/room/"+ roomId).then(function(rawMessages) {
            // success callback
            ChatServerActionCreators.receiveAll(rawMessages);
        }, function(error) {
            console.log(error);
            // Send result to dispatcher --> ChatServerActionCreators.errorReceuveAll(error);
        });
    },

    // Join socket server and set broadcastMessage callback, executes actionCreator
    joinServer: function(roomId) {

        var chat = $.connection.chatHub;

        chat.client.broadcastMessage = function(name, content, room, type, time) {

            var message = {
                Name: name,
                Content: content,
                Room: room,
                Type: type,
                Timestamp: new Date(time)
            };

            console.log(message);

            ChatServerActionCreators.receiveMessage(message);
        };

        $.connection.hub.start().done(function() {
            console.log('joinroom', roomId);
            chat.server.joinRoom(roomId);
        });
    },

    // Create new message and broadcast to socket server, should execute a result
    socketCreateMessage: function(user, message, room, type, date) {
        $.connection.hub.url = "http://slackie.azurewebsites.net/signalr";
        $.connection.hub.start().done(function () {
            var chat = $.connection.chatHub;
            chat.server.send(user, message, room, type, date);

            // Send result to dispatcher --> ChatServerActionCreators.socketCreateMessage()
        });
    }

};
