import keyMirror from 'keymirror';

module.exports = {

  ActionTypes: keyMirror({
    RECEIVE_RAW_MESSAGES: null,
    RECEIVE_RAW_MESSAGE: null,
  })

};
