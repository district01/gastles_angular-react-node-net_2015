// Vendor
import {EventEmitter} from 'events';
import assign from 'object-assign';
import _ from 'lodash';

// Dispatcher
import ChatAppDispatcher from '../dispatcher/ChatAppDispatcher';
import ChatConstants from '../constants/ChatConstants';

// Setup
let ActionTypes = ChatConstants.ActionTypes;
let CHANGE_EVENT = 'change';

// Inital data
let _messages = {};

// Model functions
function _addMessages(rawMessages) {
    _messages = rawMessages;
}

function _addMessage(message) {
    _messages.push(message);
}

// Store
let MessageStore = assign({}, EventEmitter.prototype, {

    emitChange: function() {
        this.emit(CHANGE_EVENT);
    },

    /**
     * @param {function} callback
     */
    addChangeListener: function(callback) {
        this.on(CHANGE_EVENT, callback);
    },

    removeChangeListener: function(callback) {
        this.removeListener(CHANGE_EVENT, callback);
    },

    get: function(id) {
        return _messages[id];
    },

    getAll: function(limit) {
        limit = limit || 50;

        return _.slice(_messages, _messages.length - limit, _messages.length);
        //return _messages;
    }
});

MessageStore.dispatchToken = ChatAppDispatcher.register(function(action) {

    switch(action.type) {

        case ActionTypes.RECEIVE_RAW_MESSAGES:
            _addMessages(action.rawMessages);
            MessageStore.emitChange();
            break;

        case ActionTypes.RECEIVE_RAW_MESSAGE:
            _addMessage(action.rawMessage);
            MessageStore.emitChange();
            break;

        default:
            // do nothing
    }

});

module.exports = MessageStore;
