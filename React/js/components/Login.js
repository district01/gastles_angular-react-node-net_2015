// Vendor
import React from 'react';
import mui, {Dialog, RaisedButton, FlatButton, TextField} from 'material-ui';

// Actions
import UserActionCreators from '../actions/UserActionCreators.js';

/**
 * This component shows login button and login dialog
 */
let Login = React.createClass({

    // Login button onClick
    openDialog: function() {
        this.refs.dialogModal.show();
    },

    _handleDialogCancel: function() {
        this.refs.dialogModal.dismiss();
    },

    // Dialog submit button OnTouchTap
    _handleDialogSubmit: function() {

        let userName = this.refs.userName.getValue().trim();

        // valid userName?
        if(userName.length > 1) {
            UserActionCreators.login(userName);
            this.refs.dialogModal.dismiss();
        }
        else {
            console.log('Not valid!');
        }
    },

    render() {

        // Dialog actions
        let customActions = [
            <div>
                <FlatButton
                    key="cancel"
                    label="Cancel"
                    primary={false}
                    onTouchTap={this._handleDialogCancel} />
                <FlatButton
                    key="submit"
                    label="Submit"
                    primary={true}
                    onTouchTap={this._handleDialogSubmit} />
            </div>
        ];

        // Center login button
        let style = {
            center: {
                textAlign: 'center'
            }
        };

        return(
            <div className="content" style={style.center} >
                <RaisedButton label="Login" onClick={this.openDialog} />
                <Dialog
                  title="Who are you?"
                  actions={customActions}
                  actionFocus="submit"
                  modal={true}
                  ref="dialogModal" >
                  <TextField ref="userName" hintText="Your name" />
                </Dialog>
            </div>
        );
    }
});

export default Login;