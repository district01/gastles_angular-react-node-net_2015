import React from 'react';
import mui, {Card, CardHeader, CardText} from 'material-ui';
import moment from 'moment';

let Message = React.createClass({
    render() {
        return (
            <Card key={this.props.message.MessageId} className="card">
                <CardHeader
                    title={this.props.message.Name}
                    subtitle={moment(this.props.message.Timestamp).fromNow()}
                    avatar="https://s3.amazonaws.com/uifaces/faces/twitter/brad_frost/128.jpg"/>

                <CardText>
                    {this.props.message.Content}
                </CardText>
            </Card>
        );
    }
});

module.exports = Message;