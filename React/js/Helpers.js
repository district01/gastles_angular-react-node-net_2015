var Helpers = (function() {

	// GET helper
    var makeRequest = function makeRequest (method, url, data) {

        return new Promise(function (resolve, reject) {

            var xhr = new XMLHttpRequest();
            xhr.open(method, url);

            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(JSON.parse(xhr.response));
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            };

            xhr.onerror = function () {
              reject({
                status: this.status,
                statusText: xhr.statusText
              });
            };

            if(method === 'POST' && data) {
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
                xhr.send(data);
            } else {
                xhr.send();
            }
        });
    };

    return {
    	makeRequest: makeRequest
    };
})();

module.exports = Helpers;