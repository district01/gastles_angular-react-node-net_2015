angular.module('users')
    .factory('messageFactory', [

        '$http',

        function($http) {

            //
            // FACTORY PROPERTIES --------------------------------------
            //

            var factory = {};

            factory.getMessagesForRoom = function getMessages(serverUrl, room) {
                return $http.get(serverUrl + '/api/messages/room/' + room);
            };

            return factory;
        }
    ]);
