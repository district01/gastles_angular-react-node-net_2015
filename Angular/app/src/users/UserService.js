(function() {
    'use strict';

    angular.module('users')
        .service('userService', ['$q', UserService]);

    /**
     * Users DataService
     * Uses embedded, hard-coded data model; acts asynchronously to simulate
     * remote data service call(s).
     *
     * @returns {{loadAll: Function}}
     * @constructor
     */
    function UserService($q) {
        var users = [{
            name: 'Football',
            room: 'football',
            image: 'assets/img/football.png'
        }, {
            name: 'Call Of Duty 4',
            room: 'cod4',
            image: 'assets/img/cod4.png'
        }, {
            name: 'Work',
            room: 'work',
            image: 'assets/img/work.png'
        }];

        // Promise-based API
        return {
            loadAllUsers: function() {
                // Simulate async nature of real remote calls
                return $q.when(users);
            }
        };
    }

})();
