(function() {

    angular
        .module('users')
        .controller('UserController', [
            'userService', '$mdSidenav', '$mdBottomSheet', '$log', '$q', 'messageFactory', '$scope', '$timeout', '$rootScope',
            UserController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function UserController(userService, $mdSidenav, $mdBottomSheet, $log, $q, messageFactory, $scope, $timeout, $rootScope) {
        var chat;
        var serverUrl = "http://slackie.azurewebsites.net/";

        $scope.selected = null;
        $scope.users = [];
        $scope.selectUser = selectUser;
        $scope.toggleList = toggleUsersList;
        $scope.showContactOptions = showContactOptions;
        $scope.messages = [];
        $scope.message = "";

        messageFactory.getMessagesForRoom(serverUrl, 'football').success(function(data) {
            $scope.messages = data;
            $timeout(function() {
                $(".chat").scrollTop(10000000);
            }, 750);
        });

        userService
            .loadAllUsers()
            .then(function(users) {
                $scope.users = [].concat(users);
                $scope.selected = users[0];
            });

        // *********************************
        // Internal methods
        // *********************************

        /**
         * First hide the bottomsheet IF visible, then
         * hide or Show the 'left' sideNav area
         */
        function toggleUsersList() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function() {
                $mdSidenav('left').toggle();
            });
        }

        /**
         * Select the current avatars
         * @param menuId
         */
        function selectUser(user) {
            $scope.selected = angular.isNumber(user) ? $scope.users[user] : user;
            $scope.toggleList();
            chat.server.joinRoom($scope.selected.room);
            messageFactory.getMessagesForRoom(serverUrl, $scope.selected.room).success(function(data) {
                $scope.messages = data;
                $timeout(function() {
                    $(".chat").scrollTop(10000000);
                }, 750);
            });
        }

        /**
         * Show the bottom sheet
         */
        function showContactOptions($event) {
            var user = $scope.selected;

            return $mdBottomSheet.show({
                parent: angular.element(document.getElementById('content')),
                templateUrl: './src/users/view/contactSheet.html',
                controller: ['$mdBottomSheet', ContactPanelController],
                controllerAs: "cp",
                bindToController: true,
                targetEvent: $event
            }).then(function(clickedItem) {
                clickedItem && $log.debug(clickedItem.name + ' clicked!');
            });

            /**
             * Bottom Sheet controller for the Avatar Actions
             */
            function ContactPanelController($mdBottomSheet) {
                this.user = user;
                this.actions = [{
                    name: 'Send love',
                    icon: 'phone',
                    icon_url: 'assets/svg/phone.svg'
                }, {
                    name: 'Send hate',
                    icon: 'twitter',
                    icon_url: 'assets/svg/twitter.svg'
                }];
                this.submitContact = function(action) {
                    $mdBottomSheet.hide(action);
                };
            }
        }

        $(function() {
            $.connection.hub.url = serverUrl + "signalr";
            // Declare a proxy to reference the hub.
            chat = $.connection.chatHub;
            // Create a function that the hub can call to broadcast messages.
            chat.client.broadcastMessage = function(name, content, room, type, time) {
                var message = {
                    Name: name,
                    Content: content,
                    Room: room,
                    Type: type,
                    Timestamp: time
                }

                $scope.messages.push(message);
                $scope.$apply();
                $(".chat").scrollTop(10000000);
            };

            // Start the connection.
            $.connection.hub.start().done(function() {
                chat.server.joinRoom("football");
                $scope.username = prompt('Enter your name:', '');
                $("body").css("display", "flex");
            });
        });

        document.onkeypress = function(e) {
            if (e.keyCode == 13) {
                if ($scope.message != '') {
                    chat.server.send($scope.username, $scope.message, $scope.selected.room, "normal", new Date());
                    $scope.message = "";
                }
            }
        }
    }

})();
